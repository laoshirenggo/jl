package com.jl.valid.error;

/**
 * 参数校验异常
 */
public class JLCheckError extends RuntimeException {
    public JLCheckError(String message) {
        super(message);
    }
}
