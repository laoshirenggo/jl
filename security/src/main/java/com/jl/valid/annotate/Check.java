package com.jl.valid.annotate;

import java.lang.annotation.*;

/**
 * 参数校验注解
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.FIELD, ElementType.PARAMETER})
public @interface Check {

    /**
     * 名称
     */
    String value() default "";

    /**
     * 最小
     */
    long max() default -1;

    /**
     * 最大
     */
    long min() default -1;

    /**
     * 正则
     */
    String regex() default "";

    /**
     * 是否有效
     */
    boolean isValid() default true;

    /**
     * 是否可为空
     */
    boolean isEmpty() default false;

    /**
     * 分组
     */
    Class<?>[] groups() default {};

}
