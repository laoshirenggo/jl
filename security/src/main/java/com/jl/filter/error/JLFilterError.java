package com.jl.filter.error;

/**
 * 过滤器异常
 */
public class JLFilterError extends RuntimeException {
    public JLFilterError(String message) {
        super(message);
    }
}
