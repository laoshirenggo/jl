package com.jl.filter.annotate;

import java.lang.annotation.*;

/**
 * 过滤器注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLFilter {
    //参数
    String[] values() default {};

    //不匹配url
    String[] excludes() default {};

    //执行顺序
    int order() default 0;

    //自定义错误信息
    String error() default "";

}
