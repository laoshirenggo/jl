package com.jl.encrypt.annotate;

import java.lang.annotation.*;

/**
 * 私钥加密注解
 * json返参
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLEncode {

}
