package com.jl.encrypt.annotate;

import com.jl.encrypt.JLRequestBodyAdvice;
import com.jl.encrypt.JLResponseBodyAdvice;
import com.jl.encrypt.config.SecretKeyConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 启用加解密注解,放启动类上
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Documented
@Import({SecretKeyConfig.class,
        JLResponseBodyAdvice.class,
        JLRequestBodyAdvice.class})
public @interface JLEnableSecurity {

}
