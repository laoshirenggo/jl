package com.jl.encrypt.annotate;

import java.lang.annotation.*;

/**
 * 私钥解密注解
 * body入参
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLDecode {

}
