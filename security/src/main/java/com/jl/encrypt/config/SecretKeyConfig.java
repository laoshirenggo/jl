package com.jl.encrypt.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "jl.encrypt")
public class SecretKeyConfig {

    private AES aes;

    private RSA rsa;

    /**
     * 是否开启加解密
     */
    private boolean open = true;

    /**
     * 是否将加解密日志输出到控制台
     */
    private boolean showLog = false;

    @Data
    public static class AES {
        /**
         * aes密钥
         */
        private String key;
    }

    @Data
    public static class RSA {
        /**
         * rsa私钥
         */
        private String privateKey;
        /**
         * rsa公钥
         */
        private String publicKey;
    }

}
