package com.jl.interceptor.error;

/**
 * 拦截器异常
 */
public class JLInrerceptorError extends RuntimeException {
    public JLInrerceptorError(String message) {
        super(message);
    }
}
