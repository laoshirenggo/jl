package com.jl.interceptor.annotate;

import java.lang.annotation.*;

/**
 * 拦截器注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLInterceptor {
    //参数
    String[] values() default {};

    //不匹配
    String[] excludes() default {};

    //执行顺序
    int order() default  0;

    //自定义错误信息
    String error() default "";

    //方法之前
    boolean before() default true;

    //方法之后
    boolean after() default false;
}
