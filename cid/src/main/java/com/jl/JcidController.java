package com.jl;

import cn.hutool.core.map.MapUtil;
import com.jcraft.jsch.Session;
import com.jl.config.JLCidConfig;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

@RestController
@RequestMapping("jcid")
public class JcidController {

    @Autowired
    private JLCidConfig jlCidConfig;

    @Autowired
    private ApplicationContext applicationContext;

    private static String line;

    private static boolean cid = false;

    @GetMapping("getServlet")
    public List<JLCidConfig.SSH> getServlet() {
        List<JLCidConfig.SSH> sshs = jlCidConfig.getSsh();
        List<JLCidConfig.SSH> result = new ArrayList<>(sshs.size());
        for (JLCidConfig.SSH ssh : sshs) {
            result.add(new JLCidConfig.SSH()
                    .setIp(ssh.getIp())
                    .setPath(ssh.getPath())
                    .setCommand(ssh.getCommand())
            );
        }
        return result;
    }

    @Async
    @GetMapping("cid")
    public void cid() {
        try {
            if (cid) {
                return;
            }
            cid = true;
            line = "";

            //打包
            String pom = System.getProperty("user.dir") + File.separator + "pom.xml";
            Runtime rt = Runtime.getRuntime();
            Process pr = rt.exec("cmd /c mvn clean & mvn install -f " + pom);
            BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream(), "GBK"));
            while (input.readLine() != null) {
                if (!StringUtils.isEmpty(input.readLine())) {
                    line += "<br>" + input.readLine();
                }
            }
            int exitVal = pr.waitFor();
            if (exitVal != 0) {
                line += "<br>[ERROR] " + "Exited with error code " + exitVal;
                return;
            }
            //解析pom，获得包名
            Model model = new MavenXpp3Reader().read(new FileReader(pom));
            String jar = model.getArtifactId() + "-" + model.getVersion() + ".jar";
            //上传文件到服务器
            List<JLCidConfig.SSH> sshs = jlCidConfig.getSsh();
            ThreadPoolTaskExecutor executor = applicationContext.getBean("jl-thread", ThreadPoolTaskExecutor.class);
            CountDownLatch latch = new CountDownLatch(sshs.size());
            line += "<br>[INFO] 开始上传";
            for (JLCidConfig.SSH ssh : sshs) {
                executor.execute(() -> {
                    try {
                        Session session = JLSSH.getSession(ssh.getIp(), ssh.getPort(), ssh.getUser(), ssh.getPassword());
                        //上传文件
                        JLSSH.upload(session, System.getProperty("user.dir") + File.separator + "target" + File.separator + jar, ssh.getPath() + "/" + jar);
                        //执行命令
                        if (!StringUtils.isEmpty(ssh.getCommand())) {
                            String exec = JLSSH.exec(session, ssh.getCommand());
                            if (!StringUtils.isEmpty(exec)) {
                                line += "<br>[INFO] " + ssh.getIp() + " exec：" + exec;
                            }
                        }
                        session.disconnect();
                        line += "<br>[INFO] " + ssh.getIp() + " success";
                    } catch (Exception e) {
                        line += "<br>[ERROR] " + e.getMessage();
                    } finally {
                        latch.countDown();
                    }
                });
            }
            latch.await();
            line += "<br>[INFO] 构建结束";
        } catch (Exception e) {
            line += "<br>[ERROR] " + e.getMessage();
        } finally {
            try {
                Thread.sleep(3000);
                cid = false;
                line = "";
            } catch (Exception es) {
                es.printStackTrace();
            }
        }
    }

    @GetMapping("getLine")
    public Map<String, Object> getLine() {
        Map<String, Object> map = new HashMap<>(2);
        return MapUtil.builder(new HashMap<String, Object>(2))
                .put("line", line)
                .put("cid", cid)
                .build();
    }

}
