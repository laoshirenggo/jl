package com.jl.annotation;

import java.lang.annotation.*;

/**
 * 订阅名注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLTopicName {
    String value();
}
