package com.jl;

import cn.hutool.json.JSONObject;
import com.hazelcast.core.*;
import com.hazelcast.map.MapInterceptor;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * hazelcast工具类
 */
@Service
@AllArgsConstructor
public class JLHazelcast<V> {

    private HazelcastInstance hazelcastInstance;

    /**
     * 写入map
     *
     * @param name
     * @param key
     * @param value
     */
    public void setMap(String name, String key, V value) {
        IMap<String, V> map = hazelcastInstance.getMap(name);
        map.put(key, value);
    }

    /**
     * 写入map
     *
     * @param name
     * @param map
     */
    public void setMap(String name, Map<String, V> map) {
        hazelcastInstance.getMap(name).putAll(map);
    }

    /**
     * 获取map
     *
     * @param name
     * @param key
     * @return
     */
    public V getMap(String name, String key) {
        IMap<String, V> map = hazelcastInstance.getMap(name);
        return map.get(key);
    }

    /**
     * 获取map
     *
     * @param name
     * @return
     */
    public Map<String, V> getMap(String name) {
        return hazelcastInstance.getMap(name);
    }

    /**
     * 获取imap
     *
     * @param name
     * @return
     */
    public IMap<String, V> getIMap(String name) {
        return hazelcastInstance.getMap(name);
    }

    /**
     * 删除mao
     *
     * @param name
     * @param key
     */
    public void removeMap(String name, String key) {
        IMap<String, V> map = hazelcastInstance.getMap(name);
        map.remove(key);
    }

    /**
     * 清空map
     *
     * @param name
     */
    public void clearMap(String name) {
        hazelcastInstance.getMap(name).clear();
    }

    /**
     * 写入list
     *
     * @param name
     * @param value
     */
    public void setList(String name, V value) {
        IList<V> clusterList = hazelcastInstance.getList(name);
        clusterList.add(value);
    }

    /**
     * 写入list
     *
     * @param name
     * @param list
     */
    public void setList(String name, List<V> list) {
        IList<V> clusterList = hazelcastInstance.getList(name);
        clusterList.addAll(list);
    }

    /**
     * 获取list
     *
     * @param name
     * @return
     */
    public List<V> getList(String name) {
        return hazelcastInstance.getList(name);
    }

    /**
     * 下标删除list
     *
     * @param name
     * @param index
     */
    public void removeIndexList(String name, int index) {
        IList<V> clusterList = hazelcastInstance.getList(name);
        clusterList.remove(index);
    }

    /**
     * 值删除list
     *
     * @param name
     * @param value
     */
    public void removeValueList(String name, V value) {
        IList<V> clusterList = hazelcastInstance.getList(name);
        clusterList.remove(value);
    }

    /**
     * 清空list
     *
     * @param name
     */
    public void clearList(String name) {
        hazelcastInstance.getList(name).clear();
    }

    /**
     * 发布消息
     *
     * @param name
     * @param jsonObject
     */
    public void sendTopic(String name, JSONObject jsonObject) {
        ITopic<Object> topic = hazelcastInstance.getTopic(name);
        topic.publish(jsonObject.toString());
    }

    /**
     * 设置消费者
     *
     * @param name
     */
    public void setTopicListener(String name, Object listener) {
        //消费者需 1.实现MessageListener接口 2.添加JLTopicName注解
        ITopic<Object> topic = hazelcastInstance.getTopic(name);
        topic.addMessageListener((MessageListener<Object>) listener);
    }

    /**
     * 增加原子变量
     *
     * @param name
     * @param value
     */
    public long setAtomicIn(String name, long value) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.alterAndGet(aLong -> atomicLong.getAndSet(aLong + value));
    }

    /**
     * 扣减原子变量
     *
     * @param name
     * @param value
     */
    public long setAtomicId(String name, long value) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.alterAndGet(aLong -> atomicLong.getAndSet(aLong - value));
    }

    /**
     * 原子变量自增
     *
     * @param name
     */
    public long setAtomicIn(String name) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.alterAndGet(aLong -> atomicLong.getAndSet(aLong + 1));
    }

    /**
     * 原子变量自减
     *
     * @param name
     */
    public long setAtomicId(String name) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.alterAndGet(aLong -> atomicLong.getAndSet(aLong - 1));
    }

    /**
     * 获取原子变量
     *
     * @param name
     * @return
     */
    public long getAtomic(String name) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.get();
    }

    /**
     * 原子变量归0
     *
     * @param name
     * @return
     */
    public long emptyAtomic(String name) {
        IAtomicLong atomicLong = hazelcastInstance.getAtomicLong(name);
        return atomicLong.getAndSet(0);
    }

    /**
     * 设置map拦截器
     *
     * @param name
     * @param interceptor
     */
    public void setMapInterceptor(String name, MapInterceptor interceptor) {
        hazelcastInstance.getMap(name).addInterceptor(interceptor);
    }
}
