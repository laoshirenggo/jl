package com.jl.runner;

import com.jl.JLHazelcast;
import com.jl.annotation.JLTopicName;
import lombok.AllArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * 消费者加载
 */
@Component
@AllArgsConstructor
public class JLHazelcastRunner implements CommandLineRunner {

    private ApplicationContext appContext;

    private JLHazelcast jlHazelcast;

    @Override
    public void run(String... args) {
        String[] beans = appContext.getBeanDefinitionNames();
        for (String bean : beans) {
            Object bean1 = appContext.getBean(bean);
            JLTopicName annotation = bean1.getClass().getAnnotation(JLTopicName.class);
            if (annotation != null) {
                jlHazelcast.setTopicListener(annotation.value(), bean1);
            }
        }
    }
}
