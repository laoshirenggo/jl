package com.jl.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component(value = "conciseMvcRegisterConfig")
public class ConciseMvcRegisterConfig implements InitializingBean {

    @Override
    public void afterPropertiesSet() {

    }

}
