package com.jl.annotation;

import java.lang.annotation.*;

/**
 * 自定义注解
 * 使用这个注解的interface将会被执行逻辑
 * 扫包路径使用实现类路径
 * @author kelvin
 *
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLMvc {

}
