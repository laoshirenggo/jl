package com.jl.annotation;

import com.jl.registermvc.ContractAutoHandlerRegisterConfiguration;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 这个是第一步
 * springboot启动时需要添加这个注解
 *
 * @author kelvin
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
@Import(ContractAutoHandlerRegisterConfiguration.class)
public @interface JLMvcApp {

    /**
     * 包扫描路径
     *
     * @return
     */
    String[] basePackages() default "";

}
