package com.jl.mapper;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

/**
 * 父mapper
 * @param <E>
 */
public interface JLMongoMapper<E> {

    /**
     * 添加
     *
     * @param entity
     * @return
     */
    E save(E entity);

    /**
     * 添加或修改
     *
     * @param entity
     * @return
     */
    E saveOrUpdate(E entity);

    /**
     * 修改
     *
     * @param query
     * @param entity
     * @return
     */
    long update(Query query, E entity);

    /**
     * 删除
     *
     * @param entity
     * @return
     */
    long remove(E entity);

    /**
     * 删除
     *
     * @param query
     * @return
     */
    long remove(Query query);

    /**
     * 统计数量
     *
     * @param query
     * @return
     */
    long count(Query query);

    /**
     * 获取并修改
     *
     * @param query
     * @param entity
     * @return
     */
    E getAndUpdate(Query query, E entity);

    /**
     * 获取并删除
     *
     * @param query
     * @return
     */
    E getAndRemove(Query query);

    /**
     * 根据id查询
     *
     * @param id
     * @return
     */
    E getById(Object id);

    /**
     * 查询对象
     *
     * @param query
     * @return
     */
    E getOne(Query query);

    /**
     * 查询集合
     *
     * @param query
     * @return
     */
    List<E> list(Query query);

    /**
     * 查询分页
     *
     * @param query
     * @param pageable
     * @return
     */
    Page<E> page(Query query, Pageable pageable);

    /**
     * 聚合查询
     *
     * @param criteria
     * @return
     */
    public JLMongoMapperImpl.AggregateWhere<E> aggregate(Criteria criteria);
}
