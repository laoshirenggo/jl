<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>io.gitee.laoshirenggo</groupId>
    <artifactId>jl</artifactId>
    <version>1.0-RELEASEOT</version>
    <modules>
        <module>mybatis-plus</module>
        <module>tools</module>
        <module>redis</module>
        <module>hazelcast</module>
        <module>security</module>
        <module>data-source</module>
        <module>spring-cloud</module>
        <module>mongo</module>
        <module>sharding-jdbc</module>
        <module>spring-factories</module>
        <module>swagger</module>
        <module>concise-mvc</module>
        <module>windows</module>
        <module>elasticsearch</module>
        <module>cid</module>
    </modules>
    <packaging>pom</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>2.3.10.RELEASE</version>
        <relativePath/> <!-- lookup parent from repository -->
    </parent>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>

        <!-- jar版本 -->
        <jl.version>1.0-RELEASEOT</jl.version>
        <aop.version>2.3.9.RELEASE</aop.version>
        <redis.version>2.3.9.RELEASE</redis.version>
        <redisson.version>3.15.4</redisson.version>
        <lombok.version>1.18.0</lombok.version>
        <feign.version>2.2.7.RELEASE</feign.version>
        <mybatis-plus.version>3.4.2</mybatis-plus.version>
        <hutool.version>5.6.3</hutool.version>
        <jjwt.version>0.9.0</jjwt.version>
        <commons-codec.version>1.15</commons-codec.version>
        <hazelcast.version>3.12.12</hazelcast.version>
        <hazelcast-spring.version>3.12.12</hazelcast-spring.version>
        <captcha.version>1.0.1</captcha.version>
        <jsch.version>0.1.54</jsch.version>
        <cglib.version>3.3.0</cglib.version>
        <mysql.version>8.0.22</mysql.version>
        <jta-atomikos.version>2.2.5.RELEASE</jta-atomikos.version>
        <druid.version>1.1.12</druid.version>
        <eureka.version>2.2.8.RELEASE</eureka.version>
        <nacos.version>2.2.5.RELEASE</nacos.version>
        <sharding-jdbc.version>4.1.0</sharding-jdbc.version>
        <liteflow.version>2.5.8</liteflow.version>
        <auto-service.version>1.0-rc6</auto-service.version>
        <swagger.version>2.0.6</swagger.version>
        <oracle.version>12.2.0.1</oracle.version>
        <es.version>2.3.10.RELEASE</es.version>
        <maven-model.version>3.8.4</maven-model.version>

        <!-- 依赖是否打包 provided=编译测试有效，运行无效 compile=编译、运行、测试时均有效 -->
        <scope.value>compile</scope.value>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>${lombok.version}</version>
        </dependency>
    </dependencies>

    <!-- 依赖版本控制 -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>tools</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>mybatis-plus</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>cloud</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>nacos</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>eureka</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>consul</artifactId>
                <version>${jl.version}</version>
            </dependency>
            <dependency>
                <groupId>io.gitee.laoshirenggo</groupId>
                <artifactId>spring-factories</artifactId>
                <version>${jl.version}</version>
            </dependency>

            <!--分布式事务-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-jta-atomikos</artifactId>
                <version>${jta-atomikos.version}</version>
            </dependency>
            <!--连接池-->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>druid</artifactId>
                <version>${druid.version}</version>
            </dependency>
            <!-- mysql -->
            <dependency>
                <groupId>mysql</groupId>
                <artifactId>mysql-connector-java</artifactId>
                <version>${mysql.version}</version>
            </dependency>
            <!-- mybatis-plus -->
            <dependency>
                <groupId>com.baomidou</groupId>
                <artifactId>mybatis-plus-boot-starter</artifactId>
                <version>${mybatis-plus.version}</version>
            </dependency>
            <!-- hutool -->
            <dependency>
                <groupId>cn.hutool</groupId>
                <artifactId>hutool-all</artifactId>
                <version>${hutool.version}</version>
            </dependency>
            <!-- hazelcast -->
            <dependency>
                <groupId>com.hazelcast</groupId>
                <artifactId>hazelcast</artifactId>
                <version>${hazelcast.version}</version>
            </dependency>
            <dependency>
                <groupId>com.hazelcast</groupId>
                <artifactId>hazelcast-spring</artifactId>
                <version>${hazelcast-spring.version}</version>
            </dependency>
            <!-- jsch -->
            <dependency>
                <groupId>com.jcraft</groupId>
                <artifactId>jsch</artifactId>
                <version>${jsch.version}</version>
            </dependency>
            <!--redis-->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-redis</artifactId>
                <version>${redis.version}</version>
            </dependency>
            <!--redisson-->
            <dependency>
                <groupId>org.redisson</groupId>
                <artifactId>redisson</artifactId>
                <version>${redisson.version}</version>
            </dependency>
            <!-- aop -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-aop</artifactId>
                <version>${aop.version}</version>
            </dependency>
            <!-- jwt -->
            <dependency>
                <groupId>io.jsonwebtoken</groupId>
                <artifactId>jjwt</artifactId>
                <version>${jjwt.version}</version>
            </dependency>
            <!-- 验证码 -->
            <dependency>
                <groupId>com.ramostear</groupId>
                <artifactId>Happy-Captcha</artifactId>
                <version>${captcha.version}</version>
            </dependency>
            <!--shardingsphere start-->
            <dependency>
                <groupId>org.apache.shardingsphere</groupId>
                <artifactId>sharding-jdbc-spring-boot-starter</artifactId>
                <version>${sharding-jdbc.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.shardingsphere</groupId>
                <artifactId>sharding-jdbc-spring-namespace</artifactId>
                <version>${sharding-jdbc.version}</version>
            </dependency>
            <!-- eureka server依赖 -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
                <version>${eureka.version}</version>
            </dependency>
            <!-- eureka client依赖 -->
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
                <version>${eureka.version}</version>
            </dependency>
            <!-- nacos依赖 -->
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
                <version>${nacos.version}</version>
            </dependency>
            <dependency>
                <groupId>com.alibaba.cloud</groupId>
                <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
                <version>${nacos.version}</version>
            </dependency>
            <!-- 扫描依赖 -->
            <dependency>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service-annotations</artifactId>
                <version>${auto-service.version}</version>
            </dependency>
            <dependency>
                <groupId>com.google.auto.service</groupId>
                <artifactId>auto-service</artifactId>
                <version>${auto-service.version}</version>
            </dependency>
            <!-- commons-codec -->
            <dependency>
                <groupId>commons-codec</groupId>
                <artifactId>commons-codec</artifactId>
                <version>${commons-codec.version}</version>
            </dependency>
            <!-- cglib -->
            <dependency>
                <groupId>cglib</groupId>
                <artifactId>cglib</artifactId>
                <version>${cglib.version}</version>
            </dependency>
            <!-- swagger -->
            <dependency>
                <groupId>com.github.xiaoymin</groupId>
                <artifactId>knife4j-spring-boot-starter</artifactId>
                <version>${swagger.version}</version>
            </dependency>
            <!-- oracle -->
            <dependency>
                <groupId>com.github.noraui</groupId>
                <artifactId>ojdbc8</artifactId>
                <version>${oracle.version}</version>
            </dependency>
            <!-- es -->
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-data-elasticsearch</artifactId>
                <version>${es.version}</version>
            </dependency>
            <!-- maven pom解析工具 -->
            <dependency>
                <groupId>org.apache.maven</groupId>
                <artifactId>maven-model</artifactId>
                <version>${maven-model.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <profiles>
        <profile>
            <!-- profile-id在这 -->
            <id>jl</id>
            <build>
                <plugins>
                    <!-- 要将源码放上去 -->
                    <plugin>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>2.4</version>
                        <configuration>
                            <attach>true</attach>
                        </configuration>
                        <executions>
                            <execution>
                                <phase>compile</phase>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- javadoc -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>2.10.4</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                                <configuration>
                                    <aggregate>true</aggregate>
                                    <additionalparam>-Xdoclint:none</additionalparam>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- GPG -->
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.5</version>
                        <executions>
                            <execution>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <!-- 说明 -->
    <description>依赖定义</description>
    <!-- 文档地址 -->
    <url>https://gitee.com/laoshirenggo/jl</url>
    <!-- 开源协议 -->
    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
        </license>
    </licenses>
    <!-- git地址 -->
    <scm>
        <connection>scm:git:gitee.com/laoshirenggo/jl.git</connection>
        <developerConnection>scm:git:gitee.com/laoshirenggo/jl.git</developerConnection>
        <url>git@gitee.com/laoshirenggo/jl.git</url>
    </scm>
    <!-- 作者信息 -->
    <developers>
        <developer>
            <name>lijin</name>
            <email>467640597@qq.com</email>
        </developer>
    </developers>
    <!-- 上传包的配置 -->
    <distributionManagement>
        <repository>
            <id>snapshots</id>
            <url>https://s01.oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
        <snapshotRepository>
            <id>snapshots</id>
            <url>https://s01.oss.sonatype.org/content/repositories/snapshots/</url>
        </snapshotRepository>
    </distributionManagement>

</project>