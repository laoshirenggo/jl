package com.jl.springbean;

import java.lang.reflect.Type;

/**
 * 代理实现抽象为继承类
 *
 * @param <A>
 * @param <B>
 */
public class JLInvocationHandler3<A, B, C> extends JLInvocationHandler2<A, B> {

    protected Class<C> classc;

    public JLInvocationHandler3(Class interfaceType) {
        super(interfaceType);
        Type[] actualType = getGenericityType(interfaceType);
        // 取数组的第3个，肯定是C的类型
        this.classc = (Class<C>) actualType[2];
    }

}
