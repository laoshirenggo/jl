package com.jl.springbean.annotation;

import java.lang.annotation.*;

/**
 * spring接口代理注解
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLProxy {

}
