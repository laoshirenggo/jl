package com.jl.springbean;

import java.lang.reflect.Type;

/**
 * 代理实现抽象为继承类
 *
 * @param <A>
 * @param <B>
 */
public class JLInvocationHandler2<A, B> extends JLInvocationHandler<A> {

    protected Class<B> classb;

    public JLInvocationHandler2(Class interfaceType) {
        super(interfaceType);
        Type[] actualType = getGenericityType(interfaceType);
        // 取数组的第2个，肯定是B的类型
        this.classb = (Class<B>) actualType[1];
    }

}
