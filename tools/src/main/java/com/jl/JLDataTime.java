package com.jl;

import lombok.SneakyThrows;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期工具类
 */
public class JLDataTime {

    public final static String defaultFormat = "yyyy-MM-dd HH:mm:ss";

    /**
     * 计算时间差
     *
     * @param startTime 开始日期
     * @param endTime   结束日期
     * @return
     */
    public static long diff(LocalDateTime startTime, LocalDateTime endTime) {
        Duration duration = Duration.between(startTime, endTime);
        long diff = duration.toMillis();
        return diff;
    }

    /**
     * 计算时间差
     *
     * @param startTime 开始日期
     * @param endTime   结束日期
     * @param type      结果单位 d=天 h=时 m=分 s=秒 ss=毫秒
     * @return
     */
    public static long diff(LocalDateTime startTime, LocalDateTime endTime, String type) {
        long diff = diff(startTime, endTime);
        long result = type.equals("d") ? diff / 86400000
                : type.equals("h") ? diff / 3600000
                : type.equals("m") ? diff / 60000
                : type.equals("s") ? diff / 1000
                : type.equals("ss") ? diff
                : 0;
        return result;
    }

    /**
     * 获取日期（当前）
     *
     * @param format 日期格式
     * @return
     */
    public static String getSring(String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        return df.format(new Date());
    }

    /**
     * 获取日期（当前）
     *
     * @return
     */
    public static String getSring() {
        return getSring(defaultFormat);
    }

    /**
     * 获取日期（当前）
     *
     * @return
     */
    public static LocalDateTime getLocal() {
        return LocalDateTime.now();
    }

    /**
     * 获取日期（加减时间）
     *
     * @param year   年+-
     * @param month  月+-
     * @param day    日+-
     * @param hour   时+-
     * @param minute 分+-
     * @param second 秒+-
     * @param time   指定日期
     * @return
     */
    @SneakyThrows
    public static String getStringAround(int year, int month, int day, int hour, int minute, int second, String time) {
        Calendar beforeTime = Calendar.getInstance();
        Date date = new SimpleDateFormat(defaultFormat).parse(time);
        beforeTime.setTime(date);
        beforeTime.add(Calendar.YEAR, year);
        beforeTime.add(Calendar.MONTH, month);
        beforeTime.add(Calendar.DATE, day);
        beforeTime.add(Calendar.HOUR, hour);
        beforeTime.add(Calendar.MINUTE, minute);
        beforeTime.add(Calendar.SECOND, second);
        Date beforeD = beforeTime.getTime();
        return new SimpleDateFormat(defaultFormat).format(beforeD);
    }

    /**
     * 获取日期（加减时间）
     *
     * @param year   年+-
     * @param month  月+-
     * @param day    日+-
     * @param hour   时+-
     * @param minute 分+-
     * @param second 秒+-
     * @return
     */
    public static String getStringAround(int year, int month, int day, int hour, int minute, int second) {
        return getStringAround(year, month, day, hour, minute, second, getSring());
    }

    /**
     * String转LocalDateTime
     *
     * @param time   String日期
     * @param format 格式
     * @return
     */
    public static LocalDateTime toLocal(String time, String format) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
        LocalDateTime ldt = LocalDateTime.parse(time, df);
        return ldt;
    }

    /**
     * String转LocalDateTime
     *
     * @param time String日期
     * @return
     */
    public static LocalDateTime toLocal(String time) {
        return toLocal(time, defaultFormat);
    }

    /**
     * LocalDateTime转String
     *
     * @param time   LocalDateTime日期
     * @param format 格式
     * @return
     */
    public static String toString(LocalDateTime time, String format) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern(format);
        return df.format(time);
    }

    /**
     * LocalDateTime转String
     *
     * @param time LocalDateTime日期
     * @return
     */
    public static String toString(LocalDateTime time) {
        return toString(time, defaultFormat);
    }

    /**
     * 日期字符串对比
     *
     * @param time1
     * @param time2
     * @return 正数=大于 0=相等 负数=小于
     */
    public static int stringContrast(String time1, String time2) {
        return time1.compareTo(time2);
    }

    /**
     * 获取月天数
     *
     * @param time   日期
     * @param format 格式
     * @return
     */
    @SneakyThrows
    public static int getMonthByDay(String time, String format) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new SimpleDateFormat(format).parse(time));
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取月天数
     *
     * @param time 日期
     * @return
     */
    public static int getMonthByDay(String time) {
        return getMonthByDay(time, defaultFormat);
    }
}
