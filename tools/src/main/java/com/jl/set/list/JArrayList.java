package com.jl.set.list;

import com.jl.JLLambda;
import com.jl.JLSet;

import java.util.ArrayList;
import java.util.List;

/**
 * arrayList
 *
 * @param <T>
 */
public class JArrayList<T> extends ArrayList<T> implements JList<T> {

    public JArrayList() {
    }

    public JArrayList(int initialCapacity) {
        super(initialCapacity);
    }

    public JArrayList(List<T> list) {
        super(list);
    }

    @Override
    public JList<T> set(T t) {
        this.add(t);
        return this;
    }

    @Override
    public JList<T> setAll(List<T> list) {
        this.addAll(list);
        return this;
    }

    @Override
    public <R> ToMapOper<T, R> toMap(JLLambda.JLFunction<T, R> jlFunction) {
        return new ToMapOper<>(this, jlFunction);
    }

    @Override
    public ToMapOper<T, T> toMap() {
        return new ToMapOper<>(this);
    }

    @Override
    public JList<T> comparing(JLLambda.JLFunction<T, ?>... jlFunction) {
        List<T> comparing = JLSet.list(this).comparing(jlFunction);
        return new JArrayList<T>(comparing);
    }

    @Override
    public JList<T> comparing() {
        List<T> comparing = JLSet.list(this).comparing();
        return new JArrayList<>(comparing);
    }

    @Override
    public JList<T> asc(JLLambda.JLFunction<T, ?> jlFunction) {
        List<T> asc = JLSet.list(this).asc(jlFunction);
        return new JArrayList<>(asc);
    }

    @Override
    public JList<T> asc() {
        List<T> asc = JLSet.list(this).asc();
        return new JArrayList<T>(asc);
    }

    @Override
    public JList<T> desc(JLLambda.JLFunction<T, ?> jlFunction) {
        List<T> desc = JLSet.list(this).desc(jlFunction);
        return new JArrayList<>(desc);
    }

    @Override
    public JList<T> desc() {
        List<T> desc = JLSet.list(this).desc();
        return new JArrayList<>(desc);
    }

    @Override
    public <R> JList<R> getProperty(JLLambda.JLFunction<T, R> jlFunction) {
        List<R> property = JLSet.list(this).getProperty(jlFunction);
        return new JArrayList<>(property);
    }

    @Override
    public JList<T> shuffle() {
        List<T> shuffle = JLSet.list(this).shuffle();
        return new JArrayList<>(shuffle);
    }

    @Override
    public int getIndex(T t) {
        return JLSet.list(this).getIndex(t);
    }

    @Override
    public JList<T> diff(List<T> list2) {
        List<T> diff = JLSet.list(this).diff(list2);
        return new JArrayList<>(diff);
    }

    @Override
    public JList<T> section(List<T> list2) {
        List<T> section = JLSet.list(this).section(list2);
        return new JArrayList<>(section);
    }

    @Override
    public JList<JList<T>> partition(int size) {
        List<List<T>> partition = JLSet.list(this).partition(size);
        JList<JList<T>> jList = new JArrayList<>();
        for (List<T> list : partition) {
            jList.add(new JArrayList<>(list));
        }
        return jList;
    }

    @Override
    public Filter<T> filter() {
        return new Filter<>(this);
    }

}
