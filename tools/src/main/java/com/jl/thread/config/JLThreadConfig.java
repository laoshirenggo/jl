package com.jl.thread.config;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 线程参数
 */
@Data
@Component
@ConfigurationProperties(prefix = "jl.thread")
public class JLThreadConfig {

    /**
     * 线程参数
     */
    private List<Param> param;

    @Data
    @Accessors(chain = true)
    public static class Param {
        /**
         * 核心线程数
         */
        private Integer core;
        /**
         * 最大线程数
         */
        private Integer max;
        /**
         * 队列大小
         */
        private Integer queue;
        /**
         * max - core 线程存活时间 单位：s
         */
        private Integer keep;
        /**
         * 线程名称
         */
        private String name;
    }
}
