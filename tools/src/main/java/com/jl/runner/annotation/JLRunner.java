package com.jl.runner.annotation;

import java.lang.annotation.*;

/**
 * 项目自启注解
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLRunner {
    //基本参数数组，你只需保证注解参数能强转为形参即可
    String[] value() default {};
}
