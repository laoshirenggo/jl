package com.jl;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * io工具
 */
public class JLIo {

    /**
     * 字节输入流
     *
     * @return
     */
    public static InputStreamMethod inputStream(String path) {
        try {
            InputStream inputStream;
            if ("http".equals(path.substring(0, 4))) {
                URL url = new URL(path);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("GET");
                conn.setConnectTimeout(10 * 1000);
                inputStream = conn.getInputStream();
            } else {
                inputStream = new BufferedInputStream(new FileInputStream(path));
            }
            return new InputStreamMethod(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字节输入流
     *
     * @return
     */
    public static InputStreamMethod inputStream(File file) {
        try {
            InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
            return new InputStreamMethod(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字节输出流
     *
     * @param path
     * @return
     */
    public static OutputStreamMethod outputStream(String path) {
        try {
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(path));
            return new OutputStreamMethod(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字节输出流
     *
     * @param path
     * @param append true=追加输出
     * @return
     */
    public static OutputStreamMethod outputStream(String path, boolean append) {
        try {
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(path, append));
            return new OutputStreamMethod(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字节输出流
     *
     * @param file
     * @return
     */
    public static OutputStreamMethod outputStream(File file) {
        try {
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
            return new OutputStreamMethod(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 字节输出流
     *
     * @param file
     * @param append true=追加输出
     * @return
     */
    public static OutputStreamMethod outputStream(File file, boolean append) {
        try {
            OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file, append));
            return new OutputStreamMethod(outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class InputStreamMethod {
        private InputStream inputStream;

        public InputStreamMethod(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        /**
         * 获取输入流
         *
         * @return
         */
        public InputStream get() {
            return inputStream;
        }

        /**
         * 获取文件内容
         *
         * @return
         */
        public String content() {
            try {
                StringBuffer buffer = new StringBuffer();
                byte[] bytes = new byte[1024];
                while (inputStream.read(bytes) != -1) {
                    buffer.append(new String(bytes));
                }
                return buffer.toString();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

    }

    public static class OutputStreamMethod {
        private OutputStream outputStream;

        public OutputStreamMethod(OutputStream outputStream) {
            this.outputStream = outputStream;
        }

        /**
         * 获取输出流
         *
         * @return
         */
        public OutputStream get() {
            return outputStream;
        }

        /**
         * 保存
         *
         * @param content
         */
        public void save(String content) {
            try {
                outputStream.write(content.getBytes());
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * 保存
         *
         * @param inputStream
         */
        public void save(InputStream inputStream) {
            try {
                byte[] buffer = new byte[1024];
                while (inputStream.read(buffer) != -1) {
                    outputStream.write(buffer);
                }
                outputStream.flush();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    inputStream.close();
                    outputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
