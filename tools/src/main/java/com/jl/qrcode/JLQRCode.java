package com.jl.qrcode;

import cn.hutool.core.util.IdUtil;
import com.jl.qrcode.VisualQRCode;

import java.awt.*;

/**
 * 二维码工具类
 */
public class JLQRCode {

    private static String getName() {
        return IdUtil.simpleUUID() + ".jpg";
    }

    /**
     * 圆形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     */
    public static String createCircle(String content, String bgPath, String outPath) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    new Color(0, 0, 0),
                    null,
                    null,
                    null,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_ROUND_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_CIRCLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 圆形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     * @param color   三原色
     * @return
     */
    public static String createCircle(String content, String bgPath, String outPath, Color color) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    color,
                    null,
                    null,
                    null,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_ROUND_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_CIRCLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 圆形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     * @param color   三原色
     * @param width   宽
     * @param x       x位置
     * @param y       y位置
     * @return
     */
    public static String createCircle(String content, String bgPath, String outPath, Color color, int width, int x, int y) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    color,
                    x,
                    y,
                    width,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_ROUND_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_CIRCLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 方形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     */
    public static String createSquare(String content, String bgPath, String outPath) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    new Color(0, 0, 0),
                    null,
                    null,
                    null,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 方形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     * @param color   三原色
     * @return
     */
    public static String createSquare(String content, String bgPath, String outPath, Color color) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    color,
                    null,
                    null,
                    null,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 方形
     *
     * @param content 内容
     * @param bgPath  背景图
     * @param outPath 保存路径
     * @param color   三原色
     * @param width   宽
     * @param x       x位置
     * @param y       y位置
     * @return
     */
    public static String createSquare(String content, String bgPath, String outPath, Color color, int width, int x, int y) {
        try {
            String name = getName();
            VisualQRCode.createQRCode(
                    content,
                    bgPath,
                    String.format("%s/%s", outPath, name),
                    'L',
                    color,
                    x,
                    y,
                    width,
                    true,
                    VisualQRCode.POSITION_DETECTION_SHAPE_MODEL_RECTANGLE,
                    VisualQRCode.FILL_SHAPE_MODEL_RECTANGLE);
            return name;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
