package com.jl;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud")
public class JLConsulConfig {
    /**
     * 注册中心
     */
    private Center center;
    /**
     * 业务服务
     */
    private Service service;

    @Data
    public static class Center {
        /**
         * 注册中心地址
         */
        private String url;
        /**
         * 注册中心端口
         */
        private Integer port;
        /**
         * 上报间隔，单位s(超时consul标记为服务下线)
         */
        private Integer ttlTime;
        /**
         * 服务id
         */
        private String instanceId;

        private Config config;

        @Data
        public static class Config {
            /**
             * 配置文件基本文件夹
             */
            private String prefix;
            /**
             * 配置文件文件格式
             */
            private String type;
            /**
             * 配置文件名称
             */
            private String name;
        }
    }

    @Data
    public static class Service {
        /**
         * 业务服务名
         */
        private String name;
        /**
         * 业务服务ip
         */
        private String ip;
    }

}
