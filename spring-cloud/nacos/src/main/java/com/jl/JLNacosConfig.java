package com.jl;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud")
public class JLNacosConfig {
    /**
     * 注册中心
     */
    private Center center;
    /**
     * 业务服务
     */
    private Service service;

    @Data
    public static class Center {
        /**
         * 注册中心地址
         */
        private String url;
        /**
         * 注册中心空间名
         */
        private String space;

        private Config config;

        @Data
        public static class Config {
            /**
             * 配置文件分组
             */
            private String group;
            /**
             * 配置文件文件格式
             */
            private String type;
            /**
             * 配置文件名称
             */
            private String name;
        }
    }

    @Data
    public static class Service {
        /**
         * 业务服务名
         */
        private String name;
    }

}
