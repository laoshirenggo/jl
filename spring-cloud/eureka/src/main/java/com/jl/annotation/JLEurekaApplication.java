package com.jl.annotation;

import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

import java.lang.annotation.*;

/**
 * eureka服务端启动类注解整合
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableEurekaServer
public @interface JLEurekaApplication {

}
