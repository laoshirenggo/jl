package com.jl;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud")
public class JLEurekaConfig {
    /**
     * 注册中心
     */
    private Center center;
    /**
     * 业务服务
     */
    private Service service;

    @Data
    public static class Center {
        /**
         * 注册中心地址，多个,分隔
         */
        private String url;
        /**
         * 发送心跳间隔
         */
        private Integer leaseRenewal;
        /**
         * 心跳失败踢出间隔
         */
        private Integer leaseExpiration;
        /**
         * server扫描时间
         */
        private Integer intervalTimer;
    }

    @Data
    public static class Service {
        /**
         * 业务服务名
         */
        private String name;
        /**
         * 业务服务ip
         */
        private String ip;
    }

}
