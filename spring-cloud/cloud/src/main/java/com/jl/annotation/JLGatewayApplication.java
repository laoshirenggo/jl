package com.jl.annotation;

import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

import java.lang.annotation.*;

/**
 * 网关启动类注解整合
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@EnableDiscoveryClient
@EnableHystrix
public @interface JLGatewayApplication {

}
