package com.jl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * feign 参数
 */
@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud.feign")
public class JLFeignConfig {
    /**
     * feign响应超时设置 ms
     */
    private Integer timeout;
    /**
     * feign请求头设置
     */
    private Head head;

    @Data
    public static class Head {
        /**
         * feign传递的头
         */
        private List<String> carrHeads;
        /**
         * feign设置的头
         */
        private Map<String, Object> setHeads;
    }
}
