package com.jl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud.hystrix")
public class JLHystrixConfig {
    /**
     * hystrix响应超时时间 ms
     */
    private Integer timeout;
    /**
     * hystrix线程池设置
     */
    private Threadpool threadpool;

    @Data
    public static class Threadpool {
        /**
         * 线程池核心线程数
         */
        private Integer core;
        /**
         * 最大等待队列数
         */
        private Integer queue;
    }

}
