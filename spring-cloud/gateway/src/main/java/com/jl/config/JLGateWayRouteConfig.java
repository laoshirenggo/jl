package com.jl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "jl.cloud.gateway")
public class JLGateWayRouteConfig {

    /**
     * 服务发现自匹配
     */
    private Boolean auto;

    /**
     * 路由
     */
    private Route route;

    @Data
    public static class Route {
        /**
         * 手动配置
         */
        private List<RouteParam> list;

        /**
         * 降级方法uri
         */
        private String fallBackUrl;

        @Data
        public static class RouteParam {
            /**
             * 服务名
             */
            private String serviceName;
            /**
             * 匹配路径
             */
            private String path;
            /**
             * 请求类型
             */
            private String type;
            /**
             * 降级方法uri
             */
            private String fallBackUrl;
        }
    }


}
