package com.jl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.db")
public class JLDbConfigurationProperties {
    /**
     * 地址
     */
    private String ip;
    /**
     * 端口
     */
    private Integer port;
    /**
     * 账号
     */
    private String username;
    /**
     * 密码
     */
    private String password;
    /**
     * 库名/服务名
     */
    private String service;

    /**
     * 连接池
     */
    private Hikari hikari;

    @Data
    public static class Hikari {
        /**
         * 最大连接数
         */
        private Integer maxConnect;
        /**
         * 最小空闲连接
         */
        private Integer minConnect;
        /**
         * 连接超时时间 ms
         */
        private Integer connectTimeout;
        /**
         * 空闲连接超时时间 ms
         */
        private Integer idleTimeout;
        /**
         * 连接最大存活时间 ms
         */
        private Integer maxLifetime;
    }

}
