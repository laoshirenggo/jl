package com.jl.config;

import lombok.Data;
import org.apache.ibatis.logging.Log;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.mybatis")
public class JLMybatisPlusConfigurationProperties {

    /**
     * mapper文件
     */
    private String mapper;

    /**
     * 实体类
     */
    private String entity;

    /**
     * 日志类
     */
    private Class<? extends Log> log;
}
