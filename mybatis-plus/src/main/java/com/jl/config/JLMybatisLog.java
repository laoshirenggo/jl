package com.jl.config;

import org.apache.ibatis.logging.Log;

public class JLMybatisLog implements Log {

    public JLMybatisLog(String clazz) {
        // Do Nothing
    }

    @Override
    public boolean isDebugEnabled() {
        return false;
    }

    @Override
    public boolean isTraceEnabled() {
        return false;
    }

    @Override
    public void error(String s, Throwable e) {
    }

    @Override
    public void error(String s) {
    }

    @Override
    public void debug(String s) {
    }

    @Override
    public void trace(String s) {
    }

    @Override
    public void warn(String s) {
    }
}
