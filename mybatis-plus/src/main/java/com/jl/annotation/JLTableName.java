package com.jl.annotation;

import java.lang.annotation.*;

/**
 * join关联返参对象表名注解
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLTableName {
    String value();
}
