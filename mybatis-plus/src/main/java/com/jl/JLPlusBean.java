package com.jl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * bean工具
 */
public class JLPlusBean extends JLBean {

    /**
     * 分页复制
     *
     * @param resultClass 返回集合泛型class
     * @param ipage       分页对象
     * @param <T>         泛型
     * @return
     */
    public static <T> IPage<T> copy(Class<T> resultClass, IPage<?> ipage) {
        List<T> result = copy(resultClass, ipage.getRecords());
        IPage<T> page = new Page<T>()
                .setRecords(result)
                .setPages(ipage.getPages())
                .setTotal(ipage.getTotal())
                .setSize(ipage.getSize())
                .setCurrent(ipage.getCurrent());
        return page;
    }
}
