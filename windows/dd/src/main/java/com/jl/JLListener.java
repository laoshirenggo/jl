package com.jl;

import com.melloware.jintellitype.JIntellitype;

import java.lang.reflect.Method;

/**
 * 监听工具类
 * 注：最终代码编写的项目只能打包成普通jar，不能maven打包，要把打包好的MANIFEST.MF内容替换为如下内容：
 * Manifest-Version: 1.0
 * Main-Class: 你的启动类完全限定名
 */
public class JLListener {

    /**
     * 注销按键
     *
     * @param code 自定义标识
     */
    public static void unregis(int code) {
        JIntellitype.getInstance().unregisterHotKey(code);
    }

    /**
     * 注册单按键
     *
     * @param code 自定义标识
     * @param key  监听按键key 参考 JIntellitype类getKey2KeycodeMapping()方法下的map key
     */
    public static void regis(int code, String key) {
        JIntellitype.getInstance().registerHotKey(code, key);
    }

    /**
     * 监听
     *
     * @param classz     回调类
     * @param methodName 回调方法
     */
    public static void listener(Class<?> classz, String methodName) {
        //code=自定义标识
        JIntellitype.getInstance().addHotKeyListener(code -> {
            Method[] methods = classz.getMethods();
            for (Method method : methods) {
                if (!method.getName().equals(methodName)) {
                    continue;
                }
                try {
                    method.invoke(classz.newInstance(), code);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * 按下按键
     *
     * @param ddcode 被按下dd码
     * @param code   当前监听自定义标识
     * @param key    当前监听按键key
     */
    public static void key(int ddcode, int code, String key) {
        unregis(code);
        new Thread(() -> {
            JLDDUtil.DD.instance.DD_key(ddcode, 1);
            JLDDUtil.DD.instance.DD_key(ddcode, 2);
            regis(code, key);
        }).start();
    }

}
