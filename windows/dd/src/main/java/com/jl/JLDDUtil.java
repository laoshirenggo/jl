package com.jl;

import com.sun.jna.Library;
import com.sun.jna.Native;

/**
 * dd工具类
 */
public class JLDDUtil {

    /**
     * dd驱动类
     */
    public interface DD extends Library {
        //需要把DD-64.dll或DD-32.dll放入c盘根目录
        JLDDUtil.DD instance = (JLDDUtil.DD) Native.loadLibrary("c://DD-64", JLDDUtil.DD.class);

        int DD_mov(int x, int y);

        int DD_movR(int dx, int dy);

        int DD_btn(int btn);

        int DD_whl(int whl);

        /**
         * 键事件
         *
         * @param ddcode dd码
         * @param flag   1=按下 2=抬起
         * @return
         */
        int DD_key(int ddcode, int flag);

        int DD_str(String s);
    }

    /**
     * 获取dd码
     *
     * @param key 监听按键key
     * @return
     */
    public static Integer getdd(String key) {
        Integer keys = null;
        switch (key) {
            case "1":
                keys = 201;
                break;
            case "2":
                keys = 202;
                break;
            case "3":
                keys = 203;
                break;
            case "4":
                keys = 204;
                break;
            case "5":
                keys = 205;
                break;
            case "6":
                keys = 206;
                break;
            case "7":
                keys = 207;
                break;
            case "8":
                keys = 208;
                break;
            case "9":
                keys = 209;
                break;
            case "0":
                keys = 210;
                break;

            case "q":
                keys = 301;
                break;
            case "w":
                keys = 302;
                break;
            case "e":
                keys = 303;
                break;
            case "r":
                keys = 304;
                break;
            case "t":
                keys = 305;
                break;
            case "y":
                keys = 306;
                break;
            case "u":
                keys = 307;
                break;
            case "i":
                keys = 308;
                break;
            case "o":
                keys = 309;
                break;
            case "p":
                keys = 310;
                break;

            case "a":
                keys = 401;
                break;
            case "s":
                keys = 402;
                break;
            case "d":
                keys = 403;
                break;
            case "f":
                keys = 404;
                break;
            case "g":
                keys = 405;
                break;
            case "h":
                keys = 406;
                break;
            case "j":
                keys = 407;
                break;
            case "k":
                keys = 408;
                break;
            case "l":
                keys = 409;
                break;

            case "z":
                keys = 501;
                break;
            case "x":
                keys = 502;
                break;
            case "c":
                keys = 503;
                break;
            case "v":
                keys = 504;
                break;
            case "b":
                keys = 505;
                break;
            case "n":
                keys = 506;
                break;
            case "m":
                keys = 507;
                break;

            case "f1":
                keys = 101;
                break;
            case "f2":
                keys = 102;
                break;
            case "f3":
                keys = 103;
                break;
            case "f4":
                keys = 104;
                break;
            case "f5":
                keys = 105;
                break;
            case "f6":
                keys = 106;
                break;
            case "f7":
                keys = 107;
                break;
            case "f8":
                keys = 108;
                break;
            case "f9":
                keys = 109;
                break;
            case "f10":
                keys = 110;
                break;
            case "f11":
                keys = 111;
                break;
            case "f12":
                keys = 112;
                break;
            case "space":
                keys = 603;
                break;
        }
        return keys;
    }
}
