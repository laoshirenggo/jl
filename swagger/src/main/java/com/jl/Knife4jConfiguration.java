package com.jl;

import com.github.xiaoymin.knife4j.spring.annotations.EnableKnife4j;
import com.github.xiaoymin.knife4j.spring.extension.OpenApiExtensionResolver;
import com.jl.config.Knife4jConfig;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

@Configuration
@EnableSwagger2WebMvc
@EnableKnife4j
@AllArgsConstructor
public class Knife4jConfiguration {

    private OpenApiExtensionResolver openApiExtensionResolver;

    private Knife4jConfig knife4jConfig;

    @Bean
    public Docket docket() {
        knife4jConfig.setTitle(knife4jConfig.getTitle() == null ? "接口文档" : knife4jConfig.getTitle());
        knife4jConfig.setMsg(knife4jConfig.getMsg() == null ? "暂无简介" : knife4jConfig.getMsg());
        knife4jConfig.setVersion(knife4jConfig.getVersion() == null ? "v1.0" : knife4jConfig.getVersion());
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName(knife4jConfig.getTitle() == null ? "接口文档" : knife4jConfig.getTitle())
                .select()
                .apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
                .apis(RequestHandlerSelectors.withMethodAnnotation(ApiOperation.class))
                .build()
                .extensions(openApiExtensionResolver.buildExtensions(knife4jConfig.getTitle()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(knife4jConfig.getTitle(),
                knife4jConfig.getMsg(),
                knife4jConfig.getVersion(),
                null,
                null,
                null,
                null);
    }
}
