package com.jl.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Data
@Component
@ConfigurationProperties(prefix = "jl.swagger")
public class Knife4jConfig {

    /**
     * 登录账号 默认=jl
     */
    private String username;

    /**
     * 登录密码 默认=jl
     */
    private String password;

    /**
     * 是否显示界面中SwaggerModel 默认=false
     */
    private Boolean enableSwaggerModel;

    /**
     * 是否显示界面中"文档管理"功能 默认=false
     */
    private Boolean enableDocumentManage;

    /**
     * 标题
     */
    private String title;

    /**
     * 简介
     */
    private String msg;

    /**
     * 版本号
     */
    private String version;

    /**
     * 负责人
     */
    private String contact;
}
