package com.jl;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * es条件构造器
 */
public class JLEsQuery {

    public final static String EQ = "eq", LT = "lt", GT = "gt", LE = "le", GE = "ge", LIKE = "like", NE = "ne", IN = "in", NOT_IN = "notIn";

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> getQuery(T entity) {
        return new Operate(entity, EQ);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> getQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, EQ);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> eqQuery(T entity) {
        return new Operate(entity, EQ);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> eqQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, EQ);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> ltQuery(T entity) {
        return new Operate(entity, LT);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> ltQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, LT);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> gtQuery(T entity) {
        return new Operate(entity, GT);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> gtQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, GT);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> leQuery(T entity) {
        return new Operate(entity, LE);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> leQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, LE);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> geQuery(T entity) {
        return new Operate(entity, GE);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> geQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, GE);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> likeQuery(T entity) {
        return new Operate(entity, LIKE);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> likeQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, LIKE);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> neQuery(T entity) {
        return new Operate(entity, NE);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> neQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, NE);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> inQuery(T entity) {
        return new Operate(entity, NE);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> inQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, NE);
    }

    /**
     * 获取query
     *
     * @param entity es实体对象
     * @return
     */
    public <T> Operate<T> notInQuery(T entity) {
        return new Operate(entity, NOT_IN);
    }

    /**
     * 获取query
     *
     * @param entity 入参实体对象
     * @param claszz es实体class
     * @return
     */
    public <T> Operate<T> notInQuery(Object entity, Class<T> claszz) {
        return new Operate(entity, claszz, NOT_IN);
    }

    public static class Operate<T> {
        private Object param;
        private Class<T> claszz;
        private List<String> notDefaultProperty = new ArrayList<>();
        private List<JLTuple.Tuple3<String, String, Object>> arithmetics = new ArrayList<>();
        List<JLTuple.Tuple2<String, String>> orderBy = new ArrayList<>();
        private String defaults;

        public Operate(Object param, String defaults) {
            this.param = param;
            this.defaults = defaults;
        }

        public Operate(Object param, Class<T> claszz, String defaults) {
            this.param = param;
            this.claszz = claszz;
            this.defaults = defaults;
        }

        /**
         * 默认
         */
        private BoolQueryBuilder defaults(Object param) {
            BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            List<JLTuple.Tuple3<String, Object, Class<?>>> tuple3s = JLReflect.PropertyReflect.getProperty(param);
            List<JLTuple.Tuple3<String, String, Object>> defaultArithmetics = new ArrayList<>();
            for (JLTuple.Tuple3<String, Object, Class<?>> tuple3 : tuple3s) {
                if (notDefaultProperty.contains(tuple3.getV1())) {
                    continue;
                }
                defaultArithmetics.add(new JLTuple.Tuple3<>(tuple3.getV1(), defaults, tuple3.getV2()));
            }
            arithmetic(queryBuilder, defaultArithmetics);
            return queryBuilder;
        }

        /**
         * 默认
         */
        private BoolQueryBuilder defaults(Object param, Class<T> claszz) {
            BoolQueryBuilder queryBuilder = new BoolQueryBuilder();
            List<JLTuple.Tuple3<String, Object, Class<?>>> tuple3s = JLReflect.PropertyReflect.getProperty(param);
            List<JLTuple.Tuple3<String, Object, Class<?>>> tuple3sr = JLReflect.PropertyReflect.getProperty(claszz);
            List<JLTuple.Tuple3<String, String, Object>> defaultArithmetics = new ArrayList<>();
            for (JLTuple.Tuple3<String, Object, Class<?>> tuple3 : tuple3s) {
                String property = tuple3.getV1();
                Object value = tuple3.getV2();
                if (notDefaultProperty.contains(property)) {
                    continue;
                }
                for (JLTuple.Tuple3<String, Object, Class<?>> tuple3r : tuple3sr) {
                    if (property.equals(tuple3r.getV1())) {
                        defaultArithmetics.add(new JLTuple.Tuple3<>(property, defaults, value));
                        break;
                    }
                }
            }
            arithmetic(queryBuilder, defaultArithmetics);
            return queryBuilder;
        }

        /**
         * =
         */
        public Operate<T> eq(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, EQ);
            return this;
        }

        /**
         * =
         */
        public Operate<T> eq(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            eq(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * <
         */
        public Operate<T> lt(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, LT);
            return this;
        }

        /**
         * <
         */
        public Operate<T> lt(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            lt(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * >
         */
        public Operate<T> gt(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, GT);
            return this;
        }

        /**
         * >
         */
        public Operate<T> gt(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            gt(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * <=
         */
        public Operate<T> le(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, LE);
            return this;
        }

        /**
         * <=
         */
        public Operate<T> le(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            le(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * >=
         */
        public Operate<T> ge(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, GE);
            return this;
        }

        /**
         * >=
         */
        public Operate<T> ge(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            ge(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * like
         */
        public Operate<T> like(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, LIKE);
            return this;
        }

        /**
         * like
         */
        public Operate<T> like(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            like(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * ne
         */
        public Operate<T> ne(JLLambda.JLFunction<T, ?> jlFunction, Object value) {
            set(jlFunction, value, NE);
            return this;
        }

        /**
         * ne
         */
        public Operate<T> ne(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            ne(jlFunction, tuple3.getV2());
            return this;
        }

        /**
         * in
         */
        public Operate<T> in(JLLambda.JLFunction<T, ?> jlFunction, List<?> value) {
            set(jlFunction, value, IN);
            return this;
        }

        /**
         * in
         */
        public Operate<T> in(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            if (tuple3.getV2() != null) {
                in(jlFunction, (List) tuple3.getV2());
            }
            return this;
        }

        /**
         * notIn
         */
        public Operate<T> notIn(JLLambda.JLFunction<T, ?> jlFunction, List<?> value) {
            set(jlFunction, value, NOT_IN);
            return this;
        }

        /**
         * notIn
         */
        public Operate<T> notIn(JLLambda.JLFunction<T, ?> jlFunction) {
            JLTuple.Tuple3<String, Object, Class<?>> tuple3 = JLReflect.PropertyReflect.getProperty(param, JLLambda.getProperty(jlFunction));
            if (tuple3.getV2() != null) {
                notIn(jlFunction, (List) tuple3.getV2());
            }
            return this;
        }

        /**
         * 正序
         */
        public Operate<T> asc(JLLambda.JLFunction<T, ?> jlFunction) {
            String property = JLLambda.getProperty(jlFunction);
            orderBy.add(new JLTuple.Tuple2<>(property, "asc"));
            return this;
        }

        /**
         * 倒序
         */
        public Operate<T> desc(JLLambda.JLFunction<T, ?> jlFunction) {
            String property = JLLambda.getProperty(jlFunction);
            orderBy.add(new JLTuple.Tuple2<>(property, "desc"));
            return this;
        }

        /**
         * 获取条件器
         *
         * @return
         */
        public BoolQueryBuilder getBoolQueryBuilder() {
            //默认
            BoolQueryBuilder queryBuilder = claszz == null ? defaults(param) : defaults(param, claszz);
            //其他
            arithmetic(queryBuilder, arithmetics);
            return queryBuilder;
        }

        /**
         * 获取构造器
         *
         * @return
         */
        public NativeSearchQuery getNativeSearchQuery() {
            BoolQueryBuilder queryBuilder = getBoolQueryBuilder();
            NativeSearchQueryBuilder searchQuery = new NativeSearchQueryBuilder();
            searchQuery.withQuery(queryBuilder);
            //排序
            if (JLEmpty.check(orderBy)) {
                for (JLTuple.Tuple2<String, String> tuple2 : orderBy) {
                    searchQuery.withSort(SortBuilders.fieldSort(tuple2.getV1()).order(tuple2.getV2().equals("asc") ? SortOrder.ASC : SortOrder.DESC));
                }
            }
            return searchQuery.build();
        }

        private void set(JLLambda.JLFunction<T, ?> jlFunction, Object value, String way) {
            String property = JLLambda.getProperty(jlFunction);
            JLTuple.Tuple3<String, String, Object> tuple3 = new JLTuple.Tuple3<>(property, way, value);
            arithmetics.add(tuple3);
            notDefaultProperty.add(property);
        }

        /**
         * 参数校验
         */
        private boolean paramCheck(Object value) {
            if (value == null) {
                return true;
            }
            if (value instanceof String) {
                if ("".equals(value.toString().trim())) {
                    return true;
                }
            }
            return false;
        }

        /**
         * 其他构造条件处理
         */
        private void arithmetic(BoolQueryBuilder queryBuilder, List<JLTuple.Tuple3<String, String, Object>> arithmetics) {
            for (JLTuple.Tuple3<String, String, Object> arithmeticObj : arithmetics) {
                String property = arithmeticObj.getV1();
                String arithmetic = arithmeticObj.getV2();
                Object value = arithmeticObj.getV3();
                if (paramCheck(value)) {
                    continue;
                }
                if (arithmetic.equals(EQ)) {
                    queryBuilder.must(QueryBuilders.termQuery(value instanceof String ? property + ".keyword" : property, value));
                    continue;
                }
                if (arithmetic.equals(LT)) {
                    queryBuilder.must(QueryBuilders.rangeQuery(property).lt(value));
                    continue;
                }
                if (arithmetic.equals(GT)) {
                    queryBuilder.must(QueryBuilders.rangeQuery(property).gt(value));
                    continue;
                }
                if (arithmetic.equals(LE)) {
                    queryBuilder.must(QueryBuilders.rangeQuery(property).lte(value));
                    continue;
                }
                if (arithmetic.equals(GE)) {
                    queryBuilder.must(QueryBuilders.rangeQuery(property).gte(value));
                    continue;
                }
                if (arithmetic.equals(LIKE)) {
                    queryBuilder.must(QueryBuilders.wildcardQuery(property + ".keyword", "*" + value.toString() + "*"));
                    continue;
                }
                if (arithmetic.equals(NE)) {
                    queryBuilder.mustNot(QueryBuilders.termQuery(value instanceof String ? property + ".keyword" : property, value));
                    continue;
                }
                if (arithmetic.equals(IN)) {
                    if (value instanceof List) {
                        List list = (List) value;
                        if (list.size() == 0) {
                            continue;
                        }
                        queryBuilder.must(QueryBuilders.termsQuery(list.get(0) instanceof String ? property + ".keyword" : property, list));
                    }
                    continue;
                }
                if (arithmetic.equals(NOT_IN)) {
                    if (value instanceof List) {
                        List list = (List) value;
                        if (list.size() == 0) {
                            continue;
                        }
                        queryBuilder.mustNot(QueryBuilders.termsQuery(list.get(0) instanceof String ? property + ".keyword" : property, list));
                    }
                    continue;
                }
            }
        }
    }
}
