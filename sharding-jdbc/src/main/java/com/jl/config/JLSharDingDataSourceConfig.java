package com.jl.config;

import lombok.Data;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingAlgorithm;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Data
@Component
@ConfigurationProperties(prefix = "jl.sharding")
public class JLSharDingDataSourceConfig {
    /**
     * 数据源配置 第一个为默认数据源，未分片的表查该数据源
     */
    private List<DataSource> dataSource;
    /**
     * 表规则配置
     */
    private List<Table> table;
    /**
     * 是否显示sql及路由
     */
    private Boolean sqlShow;

    /**
     * 数据源
     */
    @Data
    public static class DataSource {
        /**
         * ip
         */
        private String ip;
        /**
         * 端口
         */
        private Integer port;
        /**
         * 库名
         */
        private String baseName;
        /**
         * 账号
         */
        private String name;
        /**
         * 密码
         */
        private String password;
        /**
         * 数据源名称
         */
        private String dataSourceName;
    }

    /**
     * 表配置
     */
    @Data
    public static class Table {
        /**
         * 数据源名称 多个,分开
         */
        private String dataSourceName;
        /**
         * 逻辑表
         */
        private String logicTable;
        /**
         * 物理表
         */
        private String physicsTable;

        /**
         * 分表分库规则
         */
        private Rule rule;

        /**
         * 分表分库规则
         */
        @Data
        public static class Rule {
            /**
             * 分库规则
             */
            private BaseRule baseRule;
            /**
             * 分表规则
             */
            private TableRule tableRule;

            /**
             * 分库规则
             */
            @Data
            public static class BaseRule {
                /**
                 * 分库字段
                 */
                private String column;
                /**
                 * 分库算法
                 */
                private String algorithm;
                /**
                 * 分库算法类 需实现 PreciseShardingAlgorithm<T>
                 * Collection<String> 为数据源名集合
                 * PreciseShardingValue<T> 为分库字段属性对象
                 */
                private Class<? extends PreciseShardingAlgorithm> algorithmClass;
                /**
                 * 分库算法类 值
                 */
                private String algorithmClassValue;
            }

            @Data
            public static class TableRule {
                /**
                 * 分表字段
                 */
                private String column;
                /**
                 * 分表算法
                 */
                private String algorithm;
                /**
                 * 分表算法类 需实现 PreciseShardingAlgorithm<T>
                 * Collection<String> 为表名集合
                 * PreciseShardingValue<T> 为分表字段属性对象
                 */
                private Class<? extends PreciseShardingAlgorithm> algorithmClass;

                /**
                 * 分表算法类 值
                 */
                private String algorithmClassValue;
            }
        }
    }
}
