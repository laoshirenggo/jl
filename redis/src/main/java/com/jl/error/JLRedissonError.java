package com.jl.error;

/**
 * 分布锁异常
 */
public class JLRedissonError extends RuntimeException {
    public JLRedissonError(String message) {
        super(message);
    }
}
