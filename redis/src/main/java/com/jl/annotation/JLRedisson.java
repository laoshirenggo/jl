package com.jl.annotation;

import java.lang.annotation.*;

/**
 * 分布式锁
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface JLRedisson {

    String LOCK = "lock", TRY_LOCK = "tryLock";

    /**
     * key名称
     */
    String value();

    /**
     * 类型 LOCK=公平锁 TRY_LOCK=非公平锁
     */
    String type() default LOCK;

    /**
     * 等待时间，单位：秒
     */
    int waitTime() default 10;

    /**
     * 失效时间，单位：秒
     */
    int loseTime() default 10;

    /**
     * 获取锁失败提示信息
     */
    String message() default "";

}
