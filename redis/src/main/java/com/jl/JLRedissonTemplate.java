package com.jl;

import lombok.AllArgsConstructor;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.stereotype.Service;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁工具类
 */
@Service
@AllArgsConstructor
public class JLRedissonTemplate {

    RedissonClient redissonClient;

    /**
     * 获取锁对象
     *
     * @param key 锁key
     * @return
     */
    public RLock getLock(String key) {
        RLock rlock = redissonClient.getLock(key);
        return rlock;
    }

    /**
     * 加锁（公平）
     *
     * @param rlock 锁对象
     */
    public void lock(RLock rlock) {
        rlock.lock();
    }

    /**
     * 加锁（公平）
     *
     * @param rlock    锁对象
     * @param loseTime 失效时间，单位：秒
     */
    public void lock(RLock rlock, long loseTime) {
        rlock.lock(loseTime, TimeUnit.SECONDS);
    }

    /**
     * 加锁（非公平）
     *
     * @param rlock    锁对象
     * @param waitTime 等待时间，单位：秒
     */
    public boolean tryLock(RLock rlock, long waitTime) throws Exception {
        return rlock.tryLock(waitTime, TimeUnit.SECONDS);
    }

    /**
     * 加锁（非公平）
     *
     * @param rlock    锁对象
     * @param waitTime 等待时间，单位：秒
     * @param loseTime 失效时间，单位：秒
     */
    public boolean tryLock(RLock rlock, long waitTime, long loseTime) throws Exception {
        return rlock.tryLock(waitTime, loseTime, TimeUnit.SECONDS);
    }

    /**
     * 释放锁
     *
     * @param rlock 锁对象
     */
    public void unlock(RLock rlock) {
        if (rlock.isLocked() && rlock.isHeldByCurrentThread()) {
            rlock.unlock();
        }
    }
}
