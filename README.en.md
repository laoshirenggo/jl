# jl工具集

#### Description
常用开发工具的整合重写二次开发，包含基于cloud微服务解决方案整合、实现无需更改任何配置一键切换注册中心，mybatis-plus条件构造器、舍弃xml实现基于对象连表查询，mongo条件构造器，hazelcast工具类，redis工具类，基于yml的线程池以及一些其他的杂项工具类等，极大提升开发效率以及代码优雅性。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
